# CRL-to-CSV Converter

### Настройка окружения
```
python3 -m venv .venv
source .venv/bin/activate
```

### Установка требуемых пакетов
```
pip install -U pip
pip install -r requirements.txt
```

### Запуск скрипта (локальный файл)
```
python convert.py --file=<crl_path> --output=<csv_path>
```
где crl_path - путь к локальному файду CRL, csv_path - путь к файлу для результата

### Запуск скрипта (URL)
```
python convert.py --url=<url> --output=<csv_path>
```
где url - URL до файла CRL (например, http://developer.apple.com/certificationauthority/wwdrca.crl), csv_path - путь к файлу для результата