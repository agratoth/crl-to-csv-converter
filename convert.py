from argparse import ArgumentParser
from datetime import datetime
from urllib import request

from OpenSSL import crypto


def convert_datetime(dt):
    result_datetime = datetime.strptime(dt, '%Y%m%d%H%M%SZ')
    return result_datetime.strftime('%Y-%m-%d-%H-%M-%S')


parser = ArgumentParser()
parser.add_argument('-f', '--file', dest='filename', default=None)
parser.add_argument('-u', '--url', dest='url', default=None)
parser.add_argument('-o', '--output', dest='output', default=None)
args = parser.parse_args()

filedata = None
revoked_rows = []

if args.filename:
    try:
        with open(args.filename, 'rb') as buf:
            filedata = buf.read()
    except Exception as ex:
        print(f'ERROR: {str(ex)}')
elif args.url:
    try:
        response = request.urlopen(args.url)
        filedata = response.read()
        print('Downloaded CRL from url:', args.url)
    except Exception as ex:
        print(f'ERROR: {str(ex)}')

if filedata:
    try:
        crl_obj = crypto.load_crl(crypto.FILETYPE_ASN1, filedata)
        revoked_list = crl_obj.get_revoked()
        print('Total certificates revoked:', len(revoked_list))

        for revoked_cert in revoked_list:
            revoked_rows.append([
                convert_datetime(revoked_cert.get_rev_date().decode()),
                revoked_cert.get_serial().decode(),
            ])
    except Exception as ex:
        print(f'ERROR: {str(ex)}')
    
if args.output:
    counter = 0
    with open(args.output, 'w') as buf:
        for row in revoked_rows:
            buf.write(f'{row[0]};{row[1]}\n')
            counter += 1

    print('Complete! Writed lines:', counter)